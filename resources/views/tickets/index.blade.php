@extends('layouts.app')

@section('content')
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-between">
                    <h4 class="text-primary">Tickets</h4>
                </div>

                <div class="">
                    @foreach($tickets as $ticket)
                            <div class="card shadow">
                                <div class="card-body">
                                    <div>
                                        <div class="d-flex justify-content-between">
                                            <div class="text-center mt-2">
                                                <a href="{{ route('tickets.show', $ticket) }}">{!! $ticket->title !!}</a>
                                            </div>

                                            <div class="text-center mt-2">
                                                @can('complete', $ticket)
                                                    <form action="{{ route('tickets.complete', $ticket->id) }}" method="POST">
                                                        @csrf 
                                                        @method('PUT')
                                                        <button class="btn btn-outline-success btn-sm">Complete</button>
                                                    </form>
                                                @endcan

                                                @can('delete', $ticket)
                                                    <form action="{{ route('tickets.destroy', $ticket->id) }}" method="POST">
                                                        @csrf 
                                                        @method('DELETE')
                                                        <button class="btn btn-outline-danger btn-sm mr-3">Delete</button>
                                                    </form>
                                                @endcan

                                                <div class="text-center d-flex justify-content-between mt-2">  

                                                    @can('resolveUnresolve', $ticket)
                                                        <form action="{{ route('tickets.resolve', $ticket->id) }}" method="POST">
                                                            @csrf 
                                                            @method('PUT')
                                                            <button class="btn btn-outline-success btn-sm mr-3">Resolve</button>
                                                        </form>

                                                        <form action="{{ route('tickets.unresolve', $ticket->id) }}" method="POST">
                                                            @csrf 
                                                            @method('PUT')
                                                            <button class="btn btn-outline-danger btn-sm">Unresolve</button>
                                                        </form>
                                                    @endcan

                                                </div>
                                            </div>

                                        </div>

                                        <div class="mt-2">
                                            {!! $ticket->description !!}
                                        </div>

                                        <div class="mt-2">
                                            <small>
                                                    @if(!auth()->user()->isTeamLeader())
                                                        <a href="#" class="text-muted"><i class="fa fa-pen mr-1"></i> {{$ticket->owner->name}} 
                                                    @else
                                                        <a href="#" class="text-muted"><i class="fa fa-user mr-1"></i> {{$ticket->assigned->first()->name}} 
                                                    @endif
                                            </a><span class="text-muted">|<i class="fa fa-pencil ml-1"></i>{{$ticket->created_date}}</span>
                                            </small>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection