@extends('layouts.app')

@section('title', 'New Ticket')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Ticket</h3></div>
                    <div class="card-body">
                        <form action="{{ route('tickets.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text"
                                    id="title"
                                    name="title"
                                    placeholder="Enter Title"
                                    value = "{{ old('title') }}"
                                    class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}">
                                @error('title')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="description" 
                                    type="hidden" 
                                    name="description" 
                                    value="{{ old('description') }}">
                                <trix-editor input="description"></trix-editor>
                                @error('description')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="assignment" id="autoAssign" value="auto" checked>
                                <label class="form-check-label" for="autoAssign">
                                    Auto Assign
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="assignment" id="manualAssign" value="manual">
                                <label class="form-check-label" for="manualAssign">
                                    Manual Assign
                                </label>
                            </div>

                            <div class="form-group col-md-6 mt-4 d-none p-0" id="team-members">
                                <label for="manual_assign_id">Members</label>
                                <select id="manual_assign_id" class="form-control" name="manual_assign_id">
                                    <option selected disabled>Choose...</option>
                                    @foreach($teamMembers as $teamMember)
                                        <option value="{{ $teamMember->id }}">{{ $teamMember->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-success mt-4">Assign</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
    <script>
        var manualAssign = document.getElementById('manualAssign');
        manualAssign.addEventListener('change', function(){
            var teamMembers = document.getElementById('team-members');
            if(this.checked == true){
                // console.log(teamMembers);
                teamMembers.classList.toggle('d-none');
            }   
        });

        var autoAssign = document.getElementById('autoAssign');
        autoAssign.addEventListener('change', function(){
            var teamMembers = document.getElementById('team-members');
            if(this.checked == true){
                // console.log(teamMembers);
                teamMembers.classList.add('d-none');
            }   
        });
    </script>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
@endsection
