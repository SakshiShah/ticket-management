@extends('layouts.app')

@section('title', 'Home')

@section('content')
<div class="container mt-3">
    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('user.best-employee') }}" method="POST">
                @csrf
                    <label for="best_employee">Best Employee</label>
                    <div class="form-inline">
                        <div class="form-group">
                            <select class="form-control mr-4" id="best_employee" name="best_employee">
                                <option value="week">Current Week</option>
                                <option value="month">Current Month</option>
                                <option value="year">Current Year</option>
                            </select>
                        </div>
                        <div class="">
                            <button class="btn btn-outline-secondary btn-round" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
            </form>
        </div>  

        <div class="col-md-4">
        </div>
    </div>
</div>
@endsection
