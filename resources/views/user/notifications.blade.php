@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h5>Notifications</h5></div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($notifications as $notification)

                                <li class="list-group-item">
                                    @if($notification->type === "App\Notifications\TicketCompleted")
                                        Your Ticket was completed!
                                        <a href="{{route('tickets.show', $notification->data['ticket']['id'])}}" class="float-right btn btn-sm btn-info text-white">View Ticket</a>
                                    @endif

                                    @if($notification->type === "App\Notifications\TicketResolved")
                                        Your Ticket was resolved!
                                        <a href="{{route('tickets.show', $notification->data['ticket']['id'])}}" class="float-right btn btn-sm btn-info text-white">View Ticket</a>
                                    @endif

                                    @if($notification->type === "App\Notifications\TicketUnresolved")
                                        Your Ticket was unresolved!
                                        <a href="{{route('tickets.show', $notification->data['ticket']['id'])}}" class="float-right btn btn-sm btn-info text-white">View Ticket</a>
                                    @endif

                                    @if($notification->type === "App\Notifications\TicketAssigned")
                                        A Ticket has been assigned to you!
                                        <a href="{{route('tickets.show', $notification->data['ticket']['id'])}}" class="float-right btn btn-sm btn-info text-white">View Ticket</a>
                                        </strong>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection