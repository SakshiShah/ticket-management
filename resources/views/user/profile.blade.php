@extends('layouts.app')

@section('title', 'Profile')

@section('content')
<div class="header pb-6 d-flex align-items-center mt--3" style="min-height: 500px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
    <div class="row">
        <div class="col-lg-7 col-md-10">
        
        <h1 class="display-2 text-white">Hello {{substr($user->name, 0, strpos($user->name, ' '))}}</h1>
        <p class="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
        </div>
    </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
    <div class="col-xl-4 order-xl-2">
        <div class="card card-profile">
            <img src="{{ asset('assets/img/theme/img-1-1000x600.jpg')}}" alt="Image placeholder" class="card-img-top">
            <div class="row justify-content-center">
                <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                    <a href="#">
                    <img src="{{ \Thomaswelton\LaravelGravatar\Facades\Gravatar::src($user->email) }}" class="rounded-circle">
                    </a>
                </div>
                </div>
            </div>
            <div class="card-body pt-0 mt-5">
                <div class="text-center">
                <h5 class="h3">
                    {{ $user->name }}
                </h5>
                <div class="h5 font-weight-300">
                    <i class="ni location_pin mr-2"></i>Bucharest, Romania
                </div>
                <div class="h5 mt-4">
                    <i class="ni business_briefcase-24 mr-2"></i>Solution Manager
                </div>
                <div>
                    <i class="ni education_hat mr-2"></i>University of Computer Science
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-8 order-xl-1">
        <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
            <div class="col-8">
                <h3 class="mb-0">Edit profile </h3>
            </div>
            </div>
        </div>
        <div class="card-body">
            <form>
            <h6 class="heading-small text-muted mb-4">User information</h6>
            <div class="pl-lg-4">
                <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                    <label class="form-control-label" for="input-username">Username</label>
                    <input type="text" id="input-username" class="form-control" placeholder="Username" value="{{$user->name}}" disabled>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                    <label class="form-control-label" for="input-email">Email address</label>
                    <input type="email" id="input-email" class="form-control" placeholder="{{$user->email}}" disabled>
                    </div>
                </div>
                </div>
            </div>
            <hr class="my-4" />
            <!-- Description -->
            <h6 class="heading-small text-muted mb-4">About me</h6>
            <div class="pl-lg-4">
                <div class="form-group">
                <label class="form-control-label">About Me</label>
                <textarea rows="4" class="form-control" placeholder="A few words about you ..." disabled>Some content about me</textarea>
                </div>
            </div>
            </form>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection