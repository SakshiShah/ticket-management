@auth
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <i class="ni ni-app text-primary display-3"></i>
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{Request::route()->getName() == 'home' ? 'active' : ''}}" href="{{ route('home') }}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
          
            @if(!auth()->user()->isTeamLeader())
            <li class="nav-item">
              <a class="nav-link {{Request::route()->getName() == 'tickets.assigned' ? 'active' : ''}}" href="{{ route('tickets.assigned') }}">
                <i class="ni ni-curved-next text-primary"></i>
                <span class="nav-link-text">Assigned</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link {{Request::route()->getName() == 'tickets.completed' ? 'active' : ''}}" href="{{ route('tickets.completed') }}">
                <i class="ni ni-like-2 text-primary"></i>
                <span class="nav-link-text">Completed</span>
              </a>
            </li>
            @endif

            @if(auth()->user()->isTeamLeader())
            <li class="nav-item">
              <a class="nav-link {{Request::route()->getName() == 'tickets.raised' ? 'active' : ''}}" href="{{route('tickets.raised')}}">
                <i class="ni ni-fat-add text-primary"></i>
                <span class="nav-link-text">Raised</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link {{Request::route()->getName() == 'tickets.toBeResolve' ? 'active' : ''}}" href="{{ route('tickets.toBeResolve') }}">
                <i class="ni ni-collection text-primary"></i>
                <span class="nav-link-text">To be resolved</span>
              </a>
            </li>
            @endif

            <li class="nav-item">
              <a class="nav-link {{Request::route()->getName() == 'tickets.resolved' ? 'active' : ''}}" href="{{ route('tickets.resolved') }}">
                <i class="ni ni-check-bold text-primary"></i>
                <span class="nav-link-text">Resolved</span>
              </a>
            </li>

            @if(auth()->user()->isTeamLeader())
            <li class="nav-item mt-5">
              <div class="text-center">
                <a href="{{ route('tickets.create') }}" class="btn btn-primary btn-round">New Ticket</a>
              </div>
            </li>
            @endif

          </ul>
        </div>
      </div>
    </div>
  </nav>
@endauth