@auth
<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
    <div class="container-fluid">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    
        <!-- Navbar links -->
        <ul class="navbar-nav align-items-center  ml-md-auto ">

            <li class="nav-item d-xl-none">
                <!-- Sidenav toggler -->
                <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
                </div>
            </li>
            <li class="nav-item d-sm-none">
                <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="{{ route('user.notifications') }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
                    <h6 class="text-sm text-muted m-0">You have <strong class="text-primary">{{$notifications->count()}}</strong> notifications.</h6>
                </div>
                <!-- List group -->
                <div class="list-group list-group-flush">
                @if(isset($notifications))
                    @foreach($notifications as $notification)
                        <a href="{{route('tickets.show', $notification->data['ticket']['id'])}}" class="list-group-item list-group-item-action">
                        <div class="row align-items-center">
                            <div class="col ml-2">
                                @if($notification->type === "App\Notifications\TicketAssigned")
                                    <p class="text-sm mb-0">A Ticket has been assigned to you!</p>
                                @endif

                                @if($notification->type === "App\Notifications\TicketCompleted")
                                    <p class="text-sm mb-0">Hurray! Your Ticket was completed!</p>
                                @endif

                                @if($notification->type === "App\Notifications\TicketResolved")
                                    <p class="text-sm mb-0">Hurray! Your Ticket was resolved!</p>
                                @endif

                                @if($notification->type === "App\Notifications\TicketUnresolved")
                                    <p class="text-sm mb-0">Sorry! Your Ticket was unresolved!</p>
                                @endif

                                <div class="d-flex justify-content-between align-items-center">
                                    <div class=""></div>
                                    <div class="text-right text-muted">
                                    <small>{{ $notification->created_at->diffForHumans() }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                    @endforeach
                @endif
                    
            </li>
        </ul>
        <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src="{{ \Thomaswelton\LaravelGravatar\Facades\Gravatar::src(auth()->user()->email) }}">
                        </span>
                        <div class="media-body  ml-2  d-none d-lg-block">
                        <span class="mb-0 text-sm  font-weight-bold">{{auth()->user()->name}}</span>
                        </div>
                    </div>
                    </a>
                    <div class="dropdown-menu  dropdown-menu-right ">
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="{{ route('user.profile')}}" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
    </div>
</nav>


@endauth