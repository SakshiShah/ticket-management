<!-- app.blade.php -> body -->
<nav class="navbar navbar-expand-lg navbar-dark bg-default sticky-top">
    <!-- <div class="container"> -->
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            @auth
            <ul class="navbar-nav ml-auto">
                @if(!auth()->user()->isTeamLeader())
                    <li class="nav-item dropdown">
                        <a class="nav-link nav-link-icon dropdown-toggle" href="javascript:;" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pending</a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                        <a class="dropdown-item" href="{{ route('tickets.assigned') }}">Assigned</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('tickets.completed') }}">Completed</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('tickets.resolved') }}">Resolved</a>
                    </li>
                @endif

                @if(auth()->user()->isTeamLeader())
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{route('tickets.raised')}}">Raised</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('tickets.toBeResolve') }}">To be resolve</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-icon" href="{{ route('tickets.resolved') }}">Resolved</a>
                    </li>
                @endif

                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{ route('user.notifications') }}">
                                {{ auth()->user()->unreadNotifications->count() }} Notifications
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>  

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
            @endauth
        </div>
    <!-- </div> -->
</nav>

<main class="py-4">
@auth
    <div class="container mt-4">
        @include('layouts._message')
        <div class="row">
            <div class="col-md-12">
                @yield('content')
            </div>
        </div>
    @else
        @yield('content')
    @endauth
    </div>
</main>