<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('tickets/assigned', 'TicketsActionController@assigned')->name('tickets.assigned');
Route::put('tickets/{ticket}/complete', 'TicketsActionController@complete')->name('tickets.complete');
Route::get('tickets/completed', 'TicketsActionController@completed')->name('tickets.completed');
Route::get('tickets/resolved', 'TicketsActionController@resolved')->name('tickets.resolved');

Route::get('tickets/raised', 'TicketsActionController@raised')->name('tickets.raised');
Route::get('tickets/resolve', 'TicketsActionController@toBeResolve')->name('tickets.toBeResolve');
Route::put('tickets/{ticket}/resolve', 'TicketsActionController@resolve')->name('tickets.resolve');
Route::put('tickets/{ticket}/unresolve', 'TicketsActionController@unresolve')->name('tickets.unresolve');
Route::put('tickets/{ticket}/reassign', 'TicketsActionController@reassign')->name('tickets.reassign');

Route::resource('tickets', 'TicketsController');

Route::get('users/notifications', 'UserController@notifications')->name('user.notifications');
Route::post('users/best-employee', 'UserController@bestEmployee')->name('user.best-employee');
Route::get('users/profile', 'UserController@profile')->name('user.profile');