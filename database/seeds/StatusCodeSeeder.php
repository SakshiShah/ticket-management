<?php

use Illuminate\Database\Seeder;
use App\StatusCode;

class StatusCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusCode::create([
            'status' => 'assigned'
        ]);

        StatusCode::create([
            'status' => 'completed'
        ]);

        StatusCode::create([
            'status' => 'resolved'
        ]);

        StatusCode::create([
            'status' => 'unresolved'
        ]);
    }
}
