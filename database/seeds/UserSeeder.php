<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Sakshi Shah',
            'email'=>'sakshidshah24@gmail.com',
            'password'=>Hash::make('abcd1234'),
            'role'=>'team-leader'
        ]);

        User::create([
            'name'=>'Shraddha Keniya',
            'email'=>'shraddha@gmail.com',
            'password'=>Hash::make('abcd1234'),
            'role'=>'team-leader'
        ]);

        User::create([
            'name'=>'Harsh Kothari',
            'email'=>'harsh@gmail.com',
            'password'=>Hash::make('abcd1234'),
            'role'=>'team-leader'
        ]);

    }
}
