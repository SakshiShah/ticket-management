<?php

use App\User;
use App\Ticket;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);

        //Status Code
        $this->call(StatusCodeSeeder::class);

        // factory(User::class, 5)->create();

        // factory(Ticket::class, 15)->create();

        factory(User::class, 5)->create()
            ->each(function($user){
                $tickets = factory(Ticket::class, rand(3, 6))->create();
                foreach($tickets as $ticket){
                    $user->assigned()->attach([$ticket->id]);
                }
                foreach($user->assigned as $ticket){
                    $ticket->status()->attach([1]);
                }
            });

    }
}
