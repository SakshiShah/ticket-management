<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ticket;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {
    return [
        'title' => rtrim($faker->sentence(6, 10), '.'),
        'description' => $faker->paragraph(rand(2, 4), true),
        'user_id' => rand(1, 3)
    ];
});
