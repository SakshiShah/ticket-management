<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'user_id',
        'login_at',
        'logout_at'
    ];
    /**
     * RELATIONSHIP METHODS
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
