<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusCode extends Model
{
    protected $fillable = [
        'status'
    ];

    /**
     * HELPER FUNCTIONS
     */

    public function isAssigned()
    {
        return $this->status === 'assigned';
    }

    public function isCompleted()
    {
        return $this->status === 'completed';
    }

    public function isResolved()
    {
        return $this->status === 'resolved';
    }

    public function isUnresolved()
    {
        return $this->status === 'unresolved';
    }

    /**
     * ACCESSORS
     */
    public function getStatusStyleAttribute()
    {
        if($this->isAssigned())
            return "badge-default";
         
        if($this->isCompleted())
            return "badge-info";

        if($this->isResolved())
            return "badge-success";

        if($this->isUnresolved())
            return "badge-danger";
    }

    /**
     * RELATIONSHIP METHODS
     */
    public function ticket()
    {
        return $this->belongsToMany(Ticket::class)->withTimestamps();
    }
}
