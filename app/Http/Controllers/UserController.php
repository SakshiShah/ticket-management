<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    private $userLoggedTimeCollection;
    private $userResolvedCountCollection;
    private $result;

    public function bestEmployee(Request $request)
    {   
        $users = User::all();
        $this->userLoggedTimeCollection = collect([]);
        $this->userResolvedCountCollection = collect([]);
        $this->result = collect([]);

        foreach($users as $user)
        {
            if($user->isTeamLeader())
                continue;
            if(!$user->isTeamLeader())
            {
                if(!($user->logs->isEmpty()))
                {
                    if($request->best_employee == "week")
                    {
                        $totalLoggedInTime = DB::table('logs')
                                            ->select(DB::raw('user_id, SUM(TIMESTAMPDIFF(second, login_at, logout_at)) as totalTime'))
                                            ->whereRaw('user_id = ' . $user->id. ' AND WEEK(created_at) = WEEK(CURDATE()) AND YEAR(created_at) = YEAR(CURDATE())')
                                            ->groupBy('user_id')
                                            ->first();
                    }
                    elseif($request->best_employee == "month")
                    {
                        $totalLoggedInTime = DB::table('logs')
                                            ->select(DB::raw('user_id, SUM(TIMESTAMPDIFF(second, login_at, logout_at)) as totalTime'))
                                            ->whereRaw('user_id = ' . $user->id. ' AND MONTH(created_at) = MONTH(CURDATE()) AND YEAR(created_at) = YEAR(CURDATE())')
                                            ->groupBy('user_id')
                                            ->first();
                    }
                    elseif($request->best_employee == "year")
                    {
                        $totalLoggedInTime = DB::table('logs')
                                            ->select(DB::raw('user_id, SUM(TIMESTAMPDIFF(second, login_at, logout_at)) as totalTime'))
                                            ->whereRaw('user_id = ' . $user->id. ' AND YEAR(created_at) = YEAR(CURDATE())')
                                            ->groupBy('user_id')
                                            ->first();
                    }            

                    $this->userLoggedTimeCollection->push([
                        'user_id' => $user->id,
                        'time' => $totalLoggedInTime->totalTime
                    ]);
                }else{
                    $this->userLoggedTimeCollection->push([
                        'user_id' => $user->id,
                        'time' => 0
                    ]);
                }

                $resolvedTicketCount = 0;
                $tickets = $user->assigned;
                foreach($tickets as $ticket)
                {
                    if($ticket->status->last()->isResolved())
                    {   
                        if($request->best_employee == "week")
                        {
                            $temp = DB::table('status_code_ticket')
                                            ->select()
                                            ->whereRaw('id = ' . $ticket->status->last()['id']. " AND WEEK(created_at) = WEEK(CURDATE()) AND YEAR(created_at) = YEAR(CURDATE())");
                        }
                        elseif($request->best_employee == "month")
                        {
                            $temp = DB::table('status_code_ticket')
                                            ->select()
                                            ->whereRaw('id = ' . $ticket->status->last()['id']. " AND MONTH(created_at) = MONTH(CURDATE()) AND YEAR(created_at) = YEAR(CURDATE())");
                        }
                        elseif($request->best_employee == "year")
                        {
                            $temp = DB::table('status_code_ticket')
                                            ->select()
                                            ->whereRaw('id = ' . $ticket->status->last()['id']. " AND  YEAR(created_at) = YEAR(CURDATE())");
                        }
                        
                        $resolvedTicketCount += $temp->count();
                    }
                }

                $this->userResolvedCountCollection->push([
                    'user_id' => $user->id,
                    'count' => $resolvedTicketCount
                ]);
                // dd($this->userLoggedTimeCollection, $this->userResolvedCountCollection);

            }
        }
        if($this->userLoggedTimeCollection->isNotEmpty() && $this->userResolvedCountCollection->isNotEmpty())
        {
            $this->userLoggedTimeCollection->each(function ($item, $key) {
                $time = $item['time'];
                $count = $this->userResolvedCountCollection->get($key)['count'];

                if($count > 0)
                    $result = $time/$count;
                else
                    $result = 0;

                $this->result->push([
                    'user_id' => $item['user_id'],
                    'result' => $result
                ]);
            });
        }
        // dd($this->userLoggedTimeCollection);
        dd(User::find($this->result->sortBy('result')->last()['user_id']));
        
    }

    public static function notifications()
    {
        auth()->user()->unreadNotifications->markAsRead();

        $notifications = auth()->user()->notifications()->paginate(10);
        return $notifications;
    }

    public function profile()
    {
        $user = auth()->user();
        $notifications = UserController::notifications();
        return view('user.profile', compact([
            'user',
            'notifications'
        ]));
    }
    
}
