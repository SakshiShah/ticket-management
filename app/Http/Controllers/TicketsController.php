<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTicketRequest;
use App\Notifications\TicketAssigned;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class TicketsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth'])->only(['create', 'store', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $teamMembers = $users->where('role', 'team-member');
        $notifications = UserController::notifications();
        return view('tickets.create', compact([
            'teamMembers',
            'notifications'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTicketRequest $request)
    {
        $ticket = auth()->user()->raised()->create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => auth()->id()
        ]);

        if($request->assignment == 'auto')
        {
            $teamMember = User::where('role', 'team-member')->get()->random();
        }
        else if($request->assignment == 'manual')
        {
            $teamMember = User::find($request->manual_assign_id);
        }
        $teamMember->assigned()->attach([$ticket->id]);
        $ticket->status()->attach([1]);

        $ticket->assigned->last()->notify(new TicketAssigned($ticket));

        session()->flash('success', 'Ticket created and assigned successfully');

        return redirect(route('tickets.raised'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        $notifications = UserController::notifications();
        return view('tickets.show', compact([
            'ticket',
            'notifications'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        // dd($this);
        if($this->authorize('delete', $ticket))
        {
            $ticket->delete();
            session()->flash('success', 'Ticket has been deleted successfully!');
            return redirect(route('tickets.raised'));
        }
        abort(403);
    }
}
