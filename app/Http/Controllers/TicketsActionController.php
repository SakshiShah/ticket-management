<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReassignTicketRequest;
use App\Notifications\TicketAssigned;
use App\Notifications\TicketCompleted;
use App\Notifications\TicketResolved;
use App\Notifications\TicketUnresolved;
use App\Ticket;
use App\User;
use Illuminate\Support\Facades\DB;

class TicketsActionController extends Controller
{
    /**
     * Shows the assigned tickets of the team-member
     */
    public function assigned()
    {
        if(!auth()->user()->isTeamLeader())
        {
            //User is a team-member

            $tickets = User::find(auth()->id())->assigned;

            $assignedTickets = [];
            foreach($tickets as $ticket)
            {
                if($ticket->status->last()->isAssigned())
                    $assignedTickets[] = $ticket;
            }
            $tickets = $assignedTickets;

            $teamMembers = User::where('role', 'team-member')->get();
            
            $notifications = UserController::notifications();
            return view('tickets.index', compact([
                'tickets',
                'teamMembers',
                'notifications'
            ]));
        }else{
            abort(403);
        }
    }

    /**
     * Shows the completed tickets of the team-member
     */
    public function completed()
    {
        if(!auth()->user()->isTeamLeader())
        {
            //User is a team-member
            $tickets = User::find(auth()->id())->assigned;

            $completedTickets = [];
            foreach($tickets as $ticket)
            {
                if($ticket->status->last()->isCompleted())
                    $completedTickets[] = $ticket;
            }
            $tickets = $completedTickets;

            $teamMembers = User::where('role', 'team-member')->get();
            $notifications = UserController::notifications();
            return view('tickets.index', compact([
                'tickets',
                'teamMembers',
                'notifications'
            ]));
        }
        else
        {
            abort(403);
        }
    }

    /**
     * Perform the complete action on the ticket
     */
    public function complete(Ticket $ticket)
    {   
        $ticket->status()->attach([2]);
        $ticket->save();
        $ticket->owner->notify(new TicketCompleted($ticket));
        session()->flash('success', 'Ticket completed successfully');
        return redirect()->back();
    }

    /**
     * Shows the tickets which are assigned to the team-member and are resolved by the raised team-leader
     */
    public function resolved()
    {
        if(!auth()->user()->isTeamLeader()){
            //Team Member
            $tickets = User::find(auth()->id())->assigned;

            $resolvedTickets = [];
            foreach($tickets as $ticket)
            {
                if($ticket->status->last()->isResolved())
                    $resolvedTickets[] = $ticket;
            }
            $tickets = $resolvedTickets;
        }
        else
        {
            //Team Leader
            $tickets = User::find(auth()->id())->raised;

            $resolvedTickets = [];
            foreach($tickets as $ticket)
            {
                if($ticket->status->last()->isResolved())
                    $resolvedTickets[] = $ticket;
            }
            $tickets = $resolvedTickets;
        }

        $teamMembers = User::where('role', 'team-member')->get();
        $notifications = UserController::notifications();
        return view('tickets.index', compact([
            'tickets',
            'teamMembers',
            'notifications'
        ]));
    }

    /**
     * Perform the resolve action on the ticket
     */
    public function resolve(Ticket $ticket)
    {   
        // dd($ticket->assigned->last());
        
        $ticket->status()->attach([3]);
        $ticket->save();
        $ticket->assigned->last()->notify(new TicketResolved($ticket));
        session()->flash('success', 'Ticket resolved successfully');
        return redirect()->back();
    }

    public function unresolve(Ticket $ticket)
    {   
        $ticket->status()->attach([4]);
        $ticket->assigned->last()->notify(new TicketUnresolved($ticket));
        session()->flash('success', 'Ticket unresolved!');


        $teamMembers = User::where('role', 'team-member')->get();
        $notifications = UserController::notifications();
        return view('tickets.reassign', compact([
            'ticket',
            'teamMembers',
            'notifications'
        ]));
    }

    public function reassign(ReassignTicketRequest $request)
    {
        $ticket = Ticket::find($request->ticket);
        if($request->assignment == 'auto')
        {
            $teamMember = User::where('role', 'team-member')->get()->random();
        }
        else if($request->assignment == 'manual')
        {
            $teamMember = User::find($request->manual_assign_id);
        }
        $teamMember->assigned()->attach([$ticket->id]);
        $ticket->status()->attach([1]);
        $ticket->assigned->last()->notify(new TicketAssigned($ticket));

        session()->flash('success', 'Ticket reassign successfully');

        return redirect(route('tickets.raised'));
    }


    /**
     * Showd the tickets which are raised by the team-leader
     */
    public function raised()
    {
        if(auth()->user()->isTeamLeader())
        {
            $tickets = auth()->user()->raised;
            $assignedTickets = [];
            foreach($tickets as $ticket)
            {
                if($ticket->status->last()->isAssigned())
                    $assignedTickets[] = $ticket;
            }
            $tickets = $assignedTickets;

            $teamMembers = User::where('role', 'team-member')->get();
            $notifications = UserController::notifications();
            return view('tickets.index', compact([
                'tickets',
                'teamMembers',
                'notifications'
            ]));
        }
    }

    /**
     * Shows the completed tickets to the team-leader that are to be resolve.
     */
    public function toBeResolve()
    {
        if(auth()->user()->isTeamLeader())
        {
            $tickets = auth()->user()->raised;
            $completedTickets = [];
            foreach($tickets as $ticket)
            {
                if($ticket->status->last()->isCompleted())
                    $completedTickets[] = $ticket;
            }
            $tickets = $completedTickets;

            $teamMembers = User::where('role', 'team-member')->get();
            $notifications = UserController::notifications();
            return view('tickets.index', compact([
                'tickets',
                'teamMembers',
                'notifications'
            ]));
        }
    }
}
