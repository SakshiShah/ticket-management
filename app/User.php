<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Log;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'role', 'email', 'password',
    ];

    public $last_log_id = null;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * HELPER FUNCTIONS
     */
    public function isTeamLeader()
    {
        return $this->role === 'team-leader';
    }

    /**
     * RELATIONSHIP METHODS
     */

     //User raises the ticket
    public function raised()
    {
        return $this->hasMany(Ticket::class);
    }

    //ticket_user assignment table
    public function assigned()
    {
        return $this->belongsToMany(Ticket::class)->withTimestamps();
    }

    //User has logs
    public function logs()
    {
        return $this->hasMany(Log::class);
    }
}
