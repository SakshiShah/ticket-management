<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'title',
        'description',
        'user_id'
    ];

    /**
     * HELPER FUNCTION
     */
    

    /**
     * ACCESSORS Functions
     */
    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * RELATIONSHIP METHODS
     */

    //Ticket raised by
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    //ticket_user table
    public function assigned()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    //status_code_ticket table
    public function status()
    {
        return $this->belongsToMany(StatusCode::class, 'status_code_ticket')->withPivot('created_at')
            ->orderBy('status_code_ticket.created_at')
            ->withTimestamps();
    }
}

